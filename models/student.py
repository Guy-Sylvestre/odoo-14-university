# -*- coding utf-8 -*-

from odoo import models, fields, api

class UniversityStudent(models.Model):
    _name = 'university.student'
    
    f_name           = fields.Char('First name')
    l_name           = fields.Char('Last name')
    sex              = fields.Selection([('male', 'Male'), ('female', 'Female')])
    identity_card    = fields.Char('Identity card')
    adress           = fields.Text('Adress')
    birthday         = fields.Date('Birthday')
    registration_date = fields.Datetime('Registration Date')
    email            = fields.Char()
    phone            = fields.Char()