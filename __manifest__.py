# -*- coding utf-8 -*-
{
    'name': "university",
    
    'summary': """
        Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknow
    """,
    
    'description': """
        Long description of module's purpose
    """,
    
    'author': "Danhho Guy SYlvestre KRAKOU",
    'website': "www.linkedin.com/in/krakou-danho-guy-sylvestre-069339178",
    
    'categoriy': "Project Management",
    'version': '0.1',
    
    'depends': [
        'base'
    ],
    
    'data': [
        # 'security/ir.model.access.csv'
        'views/backend/student.xml'
    ]
}